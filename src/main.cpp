#include <cstdio>
#include <cstdlib>

#include "graphics/window.hpp"
#include "graphics/input.hpp"
#include "util/logger.h"
#include "init.hpp"

void keypress(size_t key, int action, void* userdata) {
    printf("Key: %lu %d\n", key, action);
}

void mousemove(float x, float y, void* userdata) {
    printf("Movement: %f, %f\n", x, y);
}

int main(void) {
    te_logf(TE_LOG_NORMAL, "Hello World!\n");
    te_logf(TE_LOG_NORMAL | TE_LOG_SILENT, "This has no prefix\n");
    
    te::init();
    
    te::Window win;
    te::Input input;
    input.addKeyEvent(keypress);
    input.addMouseEvent(mousemove);
    win.addInput(&input);

    while(win.isOpen()) {
        te::Window::poll();
        win.swap();
    }

    te::free();

    te_logf(TE_LOG_NORMAL, "Goodbye World!\n");
    return 0;
}
