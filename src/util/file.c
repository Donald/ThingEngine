#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

char *te_file_raw(const char *filepath) {
    FILE *file = fopen(filepath, "rb");
    if(file == NULL)
        return NULL;
    
    char   *text;
    size_t ntext;
    
    fseek(file, 0, SEEK_END);
    ntext = ftell(file);
    fseek(file, 0, SEEK_SET);

    text = (char *) malloc(ntext + 1);
    fread(text, 1, ntext, file);
    text[ntext] = '\0';
    fclose(file);

    return text;
}

void te_file_raw_free(char *raw) {

}
