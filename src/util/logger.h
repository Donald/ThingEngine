#ifndef TE_UTIL_LOGGER_H
#define TE_UTIL_LOGGER_H

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t TeLogLevel; 
#define TE_LOG_NORMAL   ((TeLogLevel)(0x00000000))
#define TE_LOG_VERBOSE  ((TeLogLevel)(0x00000001))  
#define TE_LOG_WARNING  ((TeLogLevel)(0x00000002))
#define TE_LOG_ERROR    ((TeLogLevel)(0x00000003))
#define TE_LOG_MAX      ((TeLogLevel)(0x00000004))
#define TE_LOG_SILENT   ((TeLogLevel)(0x00010000)) 

void   te_log_add_out(void *stream, TeLogLevel priority);
size_t te_logf(TeLogLevel lvl, const char *fmt, ...);

#ifdef __cplusplus
}
#endif


#endif
