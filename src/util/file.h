#ifndef TE_UTIL_FILE_H
#define TE_UTIL_FILE_H

#ifdef __cplusplus
extern "C" {
#endif

char *te_file_raw(const char *filepath);
void  te_file_raw_free(char *raw);

#ifdef __cplusplus
}
#endif

#endif
