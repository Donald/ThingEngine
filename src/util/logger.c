#include "logger.h"
#include <stdio.h>

#define MAX_OUTS 16

typedef struct {
    FILE *out;
    TeLogLevel priority;
} Stream;

static int     initialized = 0;
static Stream  streams[MAX_OUTS];
static size_t   nstreams;

static void verify_initialized( void ) {
    if(initialized)
        return;
    streams[0].out      = stdout;
    streams[0].priority = TE_LOG_MAX;
    nstreams = 1;
}


void te_log_add_out(void* stream, TeLogLevel priority) {
    if(nstreams >= MAX_OUTS)
        return;
    verify_initialized();
    streams[nstreams].out      = (FILE *) stream;
    streams[nstreams].priority = priority & 0x0000000F;
}

size_t te_logf(TeLogLevel lvl, const char* fmt, ...) {
    verify_initialized();
    va_list args;
    size_t i, chars = 0;
    for(i = 0; i < nstreams; i++) {
        if((lvl & 0x0000000F) > streams[i].priority)
            continue;
        if(!(lvl & TE_LOG_SILENT)) {
            switch(lvl & 0x0000000F) {
                case TE_LOG_NORMAL:  fprintf(streams[i].out, "[ >]: "); break;
                case TE_LOG_VERBOSE: fprintf(streams[i].out, "[>>]: "); break;
                case TE_LOG_WARNING: fprintf(streams[i].out, "[ !]: "); break;
                case TE_LOG_ERROR:   fprintf(streams[i].out, "[ X]: "); break;
                default:             fprintf(streams[i].out, "[--]: "); break;
            }
        }
        va_start(args, fmt);
        chars = vfprintf(streams[i].out, fmt, args);
        va_end(args);
    }
    return chars;
}
