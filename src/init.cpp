#include "init.hpp" 
#include "util/logger.h"

#include <GLFW/glfw3.h>

#include <cstring>
#include <unistd.h>

namespace te {
    static Library lib;

    int init() {
        te_logf(TE_LOG_NORMAL, "`te::init()`\n");
        memset(&lib, 0, sizeof(Library));

#ifdef __linux__
        char path[4096];
        if(realpath("/proc/self/exe", path) == NULL)
            return 0;
        size_t i = strlen(path) - 1;
        while(i != 0 && path[i] != '/')
            i--;
        path[i] = '\0';
        lib.exepath = std::string(path);
#elif
#error "Unsupported OS"
#endif

        if(!glfwInit())
            return 0;
        te_logf(TE_LOG_VERBOSE, "GLFW initialized\n");
        
        BsonResult res;
        lib.bson = bson_lib_default(&res, BSON_LOG_NORMAL);
        if(lib.bson == NULL || res != BSON_SUCCESS)
            return 0;
        te_logf(TE_LOG_VERBOSE, "BSON initialized %p\n", lib.bson);

        return 1;
    }
    
    void free() {
        te_logf(TE_LOG_NORMAL, "`te::free()`\n");
        bson_lib_free(&lib.bson);
        te_logf(TE_LOG_VERBOSE, "BSON free'd\n");
        glfwTerminate();
        te_logf(TE_LOG_VERBOSE, "GLFW terminated\n");
    }

    Library* libs() {
        return &lib;
    }

};
