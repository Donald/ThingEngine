#ifndef TE_INIT_HPP
#define TE_INIT_HPP

#include <bson/bson.h>
#include <string>

namespace te {
    struct Library {
        BsonLib      *bson;
        std::string   exepath;
    };

    int      init();
    void     free();
    Library* libs();
}


#endif
