#include "linalg.h"

#include <string.h>

void te_mat2_add(float *dst, const float *l, const float *r) {
    dst[0] = l[0] + r[0]; 
    dst[1] = l[1] + r[1]; 
    dst[2] = l[2] + r[2]; 
    dst[3] = l[3] + r[3]; 
}                         
                          
void te_mat2_subtract(float *dst, const float *l, const float *r) {
    dst[0] = l[0] - r[0]; 
    dst[1] = l[1] - r[1]; 
    dst[2] = l[2] - r[2]; 
    dst[3] = l[3] - r[3]; 
}                         

void te_vec2xmat2(TeVec2 *dst, const TeVec2 *l, const float *r) {
    TeVec2 v;
    v.x = r[0] * l->x + r[1] * l->y;
    v.y = r[2] * l->x + r[3] * l->y;
    *dst = v;
}

void te_mat2xmat2(float *dst, const float *l, const float *r) {
    float m[4];
    m[0] = l[0] * r[0] + l[1] * r[2];
    m[1] = l[0] * r[1] + l[1] * r[3];
    m[2] = l[2] * r[0] + l[3] * r[2];
    m[2] = l[2] * r[1] + l[3] * r[3];
    memcpy(dst, m, sizeof(float) * 4);
}

void te_mat2_transpose(float *dst, const float *m) {
    float temp = m[1];
    dst[1] = m[2];
    dst[2] = temp;
    dst[0] = m[0];
    dst[3] = m[3];
}

void te_mat2_inverse(float *dst, const float *m) {
    /* TODO: implement lol*/
}
