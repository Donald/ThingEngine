#include "linalg.h"

#include <math.h>

void te_vec3_add(TeVec3 *dst, const TeVec3 *l, const TeVec3 *r) {
    dst->x = l->x + r->x;
    dst->y = l->y + r->y;
    dst->z = l->z + r->z;
}

void te_vec3_subract(TeVec3 *dst, const TeVec3 *l, const TeVec3 *r) {
    dst->x = l->x - r->x;
    dst->y = l->y - r->y;
    dst->z = l->z - r->z;
}

float te_vec3_magnitude(const TeVec3 *v) {
    return sqrt(v->x * v->x + v->y * v->y + v->z * v->z);
}

void te_vec3_normalize(TeVec3 *dst, const TeVec3 *v) {
    float mag = te_vec3_magnitude(v);
    dst->x = v->x / mag;
    dst->y = v->y / mag;
    dst->z = v->z / mag;
}

void te_vec3_scale1(TeVec3 *dst, const TeVec3 *l, float r) {
    dst->x = l->x * r;
    dst->y = l->y * r;
    dst->z = l->z * r;
}

void te_vec3_scale3(TeVec3 *dst, const TeVec3 *l, const TeVec3 *r) {
    dst->x = l->x * r->x;
    dst->y = l->y * r->y;
    dst->z = l->z * r->y;
}

void te_vec3_cross(TeVec3 *dst, const TeVec3 *l, const TeVec3 *r) {
    dst->x = l->y * r->z - l->z * r->y;  
    dst->y = l->z * r->x - l->x * r->z;  
    dst->z = l->x * r->y - l->y * r->x;  
}

float te_vec3_dot(const TeVec3 *l, const TeVec3 *r) {
    return l->x * r->x + l->y * r->y + l->z * r->z;
}



