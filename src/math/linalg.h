#ifndef TE_MATH_LINEAR_ALGERBRA_H
#define TE_MATH_LINEAR_ALGERBRA_H

typedef struct TeVec2 {
    union { float x, u; };
    union { float y, v; };
} TeVec2;

typedef struct TeVec3 {
    union { float x, u, r; };
    union { float y, v, g; };
    union { float z,    b; };
} TeVec3;

typedef struct TeVec4 {
    union { float x, u, r; };
    union { float y, v, g; };
    union { float z,    b; };
    union { float w,    a; };
} TeVec4;

void  te_vec2_add        (TeVec2 *dst, const TeVec2 *l, const TeVec2 *r);
void  te_vec2_subract    (TeVec2 *dst, const TeVec2 *l, const TeVec2 *r);
float te_vec2_magnitude  (const TeVec2 *v);
void  te_vec2_normalize  (TeVec2 *dst, const TeVec2 *v);
void  te_vec2_scale1     (TeVec2 *dst, const TeVec2 *l, float r);
void  te_vec2_scale2     (TeVec2 *dst, const TeVec2 *l, const TeVec2 *r);
float te_vec2_dot        (const TeVec2 *l, const TeVec2 *r);

void  te_vec3_add        (TeVec3 *dst, const TeVec3 *l, const TeVec3 *r);
void  te_vec3_subract    (TeVec3 *dst, const TeVec3 *l, const TeVec3 *r);
float te_vec3_magnitude  (const TeVec3 *v);
void  te_vec3_normalize  (TeVec3 *dst, const TeVec3 *v);
void  te_vec3_scale1     (TeVec3 *dst, const TeVec3 *l, float r);
void  te_vec3_scale3     (TeVec3 *dst, const TeVec3 *l, const TeVec3 *r);
void  te_vec3_cross      (TeVec3 *dst, const TeVec3 *l, const TeVec3 *r);
float te_vec3_dot        (const TeVec3 *l, const TeVec3 *r);

void  te_vec4_add        (TeVec4 *dst, const TeVec4 *l, const TeVec4 *r);
void  te_vec4_subract    (TeVec4 *dst, const TeVec4 *l, const TeVec4 *r);
float te_vec4_magnitude  (const TeVec4 *v);
void  te_vec4_normalize  (TeVec4 *dst, const TeVec4 *v);
void  te_vec4_scale1     (TeVec4 *dst, const TeVec4 *l, float r);
void  te_vec4_scale3     (TeVec4 *dst, const TeVec4 *l, const TeVec4 *r);
void  te_vec4_cross      (TeVec4 *dst, const TeVec4 *l, const TeVec4 *r);
float te_vec4_dot        (const TeVec4 *l, const TeVec4 *r);

#define te_vec_arr(v)    ((float *)( (void *)(&v) ))
#define te_arr_vec(a, s) ((s     *)( (void *)( a) ))  

void  te_mat2_add        (float  *dst, const float  *l, const float *r);
void  te_mat2_subtract   (float  *dst, const float  *l, const float *r);
void  te_vec2xmat2       (TeVec2 *dst, const TeVec2 *l, const float *r);
void  te_mat2xmat2       (float  *dst, const float  *l, const float *r);
void  te_mat2_transpose  (float  *dst, const float  *m);
void  te_mat2_inverse    (float  *dst, const float  *m);

void  te_mat3_add        (float  *dst, const float  *l, const float *r);
void  te_mat3_subtract   (float  *dst, const float  *l, const float *r);
void  te_vec3xmat3       (TeVec3 *dst, const TeVec3 *l, const float *r);
void  te_mat3xmat3       (float  *dst, const float  *l, const float *r);
void  te_mat3_transpose  (float  *dst, const float  *m);
void  te_mat3_inverse    (float  *dst, const float  *m);

void  te_mat4_add        (float  *dst, const float  *l, const float *r);
void  te_mat4_subtract   (float  *dst, const float  *l, const float *r);
void  te_vec4xmat4       (TeVec4 *dst, const TeVec4 *l, const float *r);
void  te_mat4xmat4       (float  *dst, const float  *l, const float *r);
void  te_mat4_transpose  (float  *dst, const float  *m);
void  te_mat4_inverse    (float  *dst, const float  *m);

void  te_mat4_orthographic (float *dst, float left, float right, float top, float bottom, float near, float far);
void  te_mat3_translate    (float *dst, float x, float y);
void  te_mat3_scale        (float *dst, float x, float y);
void  te_mat3_rotate       (float *dst, float rads);

/* N */

/* ... */



#endif







