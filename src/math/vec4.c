#include "linalg.h"

#include <math.h>

void te_vec4_add(TeVec4 *dst, const TeVec4 *l, const TeVec4 *r) {
    dst->x = l->x + r->x;
    dst->y = l->y + r->y;
    dst->z = l->z + r->z;
    dst->w = l->w + r->w;
}

void te_vec4_subract(TeVec4 *dst, const TeVec4 *l, const TeVec4 *r) {
    dst->x = l->x - r->x;
    dst->y = l->y - r->y;
    dst->z = l->z - r->z;
    dst->w = l->w - r->w;
}

float te_vec4_magnitude(const TeVec4 *v) {
    return sqrt(v->x * v->x + v->y * v->y + v->z * v->z + v->w * v->w);
}

void te_vec4_normalize(TeVec4 *dst, const TeVec4 *v) {
    float mag = te_vec4_magnitude(v);
    dst->x = v->x / mag;
    dst->y = v->y / mag;
    dst->z = v->z / mag;
    dst->w = v->w / mag;
}

void te_vec4_scale1(TeVec4 *dst, const TeVec4 *l, float r) {
    dst->x = l->x * r;
    dst->y = l->y * r;
    dst->z = l->z * r;
    dst->w = l->w * r;
}

void te_vec4_scale4(TeVec4 *dst, const TeVec4 *l, const TeVec4 *r) {
    dst->x = l->x * r->x;
    dst->y = l->y * r->y;
    dst->z = l->z * r->y;
    dst->w = l->w * r->y;
}

void te_vec4_cross(TeVec4 *dst, const TeVec4 *l, const TeVec4 *r) {
    dst->x = l->y * r->z - l->z * r->y;  
    dst->y = l->z * r->x - l->x * r->z;  
    dst->z = l->x * r->y - l->y * r->x;  
    /* IDK brah, no w */
}

float te_vec4_dot(const TeVec4 *l, const TeVec4 *r) {
    return l->x * r->x + l->y * r->y + l->z * r->z + l->w * r->w;
}



