#include "linalg.h"

#include <string.h>

void te_mat3_add(float *dst, const float *l, const float *r) {
    dst[0] = l[0] + r[0]; 
    dst[1] = l[1] + r[1]; 
    dst[2] = l[2] + r[2]; 
    dst[3] = l[3] + r[3]; 
    dst[4] = l[4] + r[4]; 
    dst[5] = l[5] + r[5]; 
    dst[6] = l[6] + r[6]; 
    dst[7] = l[7] + r[7]; 
    dst[8] = l[8] + r[8]; 
}                         
                          
void te_mat3_subtract(float *dst, const float *l, const float *r) {
    dst[0] = l[0] - r[0]; 
    dst[1] = l[1] - r[1]; 
    dst[2] = l[2] - r[2]; 
    dst[3] = l[3] - r[3]; 
    dst[4] = l[4] - r[4]; 
    dst[5] = l[5] - r[5]; 
    dst[6] = l[6] - r[6]; 
    dst[7] = l[7] - r[7]; 
    dst[8] = l[8] - r[8]; 
}                         

void te_vec3xmat3(TeVec3 *dst, const TeVec3 *l, const float *r) {
    TeVec3 v;
    v.x = r[0] * l->x + r[3] * l->y + r[6] * l->z;
    v.y = r[1] * l->x + r[4] * l->y + r[7] * l->z;
    v.z = r[2] * l->x + r[5] * l->y + r[8] * l->z;
    *dst = v;
}

void te_mat3xmat3(float *dst, const float *l, const float *r) {
    float m[9];
    m[0] = l[0] * r[0] + l[3] * r[1] + l[6] * r[2]; 
    m[1] = l[1] * r[0] + l[4] * r[1] + l[7] * r[2];
    m[2] = l[2] * r[0] + l[5] * r[1] + l[8] * r[2];

    m[3] = l[0] * r[3] + l[3] * r[4] + l[6] * r[5]; 
    m[4] = l[1] * r[3] + l[4] * r[4] + l[7] * r[5];
    m[5] = l[2] * r[3] + l[5] * r[4] + l[8] * r[5];
    
    m[6] = l[0] * r[6] + l[3] * r[7] + l[6] * r[8]; 
    m[7] = l[1] * r[6] + l[4] * r[7] + l[7] * r[8];
    m[8] = l[2] * r[6] + l[5] * r[7] + l[8] * r[8];
    memcpy(dst, m, sizeof(float) * 9);
}

void te_mat3_transpose(float *dst, const float *m) {
    float temp = m[1];
    dst[1] = m[2];
    dst[2] = temp;
    dst[0] = m[0];
    dst[3] = m[3];
}

void te_mat3_inverse(float *dst, const float *m) {
    /* TODO: implement lol*/
}
