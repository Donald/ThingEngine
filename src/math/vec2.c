#include "linalg.h"

#include <math.h>

void te_vec2_add(TeVec2 *dst, const TeVec2 *l, const TeVec2 *r) {
    dst->x = l->x + r->x;
    dst->y = l->y + r->y;
}

void te_vec2_subract(TeVec2 *dst, const TeVec2 *l, const TeVec2 *r) {
    dst->x = l->x - r->x;
    dst->y = l->y - r->y;
}

float te_vec2_magnitude(const TeVec2 *v) {
    return sqrt(v->x * v->x + v->y * v->y);
}

void te_vec2_normalize(TeVec2 *dst, const TeVec2 *v) {
    float mag = te_vec2_magnitude(v);
    dst->x = v->x / mag;
    dst->y = v->y / mag;
}

void te_vec2_scale1(TeVec2 *dst, const TeVec2 *l, float r) {
    dst->x = l->x * r;
    dst->y = l->y * r;
}

void te_vec2_scale2(TeVec2 *dst, const TeVec2 *l, const TeVec2 *r) {
    dst->x = l->x * r->x;
    dst->y = l->y * r->y;
}

float te_vec2_dot(const TeVec2 *l, const TeVec2 *r) {
    return l->x * r->x + l->y * r->y;
}
