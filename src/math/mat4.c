#include "linalg.h"

#include <string.h>

void te_mat4_add(float *dst, const float *l, const float *r) {
    dst[ 0] = l[ 0] + r[ 0]; 
    dst[ 1] = l[ 1] + r[ 1]; 
    dst[ 2] = l[ 2] + r[ 2]; 
    dst[ 3] = l[ 3] + r[ 3]; 
    dst[ 4] = l[ 4] + r[ 4]; 
    dst[ 5] = l[ 5] + r[ 5]; 
    dst[ 6] = l[ 6] + r[ 6]; 
    dst[ 7] = l[ 7] + r[ 7]; 
    dst[ 8] = l[ 8] + r[ 8]; 
    dst[ 9] = l[ 9] + r[ 9]; 
    dst[10] = l[10] + r[10]; 
    dst[11] = l[11] + r[11]; 
    dst[12] = l[12] + r[12]; 
    dst[13] = l[13] + r[13]; 
    dst[14] = l[14] + r[14]; 
    dst[15] = l[15] + r[15]; 
}                         
                          
void te_mat4_subtract(float *dst, const float *l, const float *r) {
    dst[ 0] = l[ 0] - r[ 0]; 
    dst[ 1] = l[ 1] - r[ 1]; 
    dst[ 2] = l[ 2] - r[ 2]; 
    dst[ 3] = l[ 3] - r[ 3]; 
    dst[ 4] = l[ 4] - r[ 4]; 
    dst[ 5] = l[ 5] - r[ 5]; 
    dst[ 6] = l[ 6] - r[ 6]; 
    dst[ 7] = l[ 7] - r[ 7]; 
    dst[ 8] = l[ 8] - r[ 8]; 
    dst[ 9] = l[ 9] - r[ 9]; 
    dst[10] = l[10] - r[10]; 
    dst[11] = l[11] - r[11]; 
    dst[12] = l[12] - r[12]; 
    dst[13] = l[13] - r[13]; 
    dst[14] = l[14] - r[14]; 
    dst[15] = l[15] - r[15]; 
}                         

void te_vec4xmat4(TeVec4 *dst, const TeVec4 *l, const float *r) {
    TeVec4 v;
    v.x = r[ 0] * l->x + r[ 4] * l->y + r[ 8] * l->z + r[12] * l->w;
    v.x = r[ 1] * l->x + r[ 5] * l->y + r[ 9] * l->z + r[13] * l->w;
    v.x = r[ 2] * l->x + r[ 6] * l->y + r[10] * l->z + r[14] * l->w;
    v.x = r[ 3] * l->x + r[ 7] * l->y + r[11] * l->z + r[15] * l->w;
    *dst = v;
}

void te_mat4xmat4(float *dst, const float *l, const float *r) {
    float m[16];
    m[ 0] = l[ 0] * r[ 0] + l[ 4] * r[ 1] + l[ 8] * r[ 2] + l[12] * r[ 3]; 
    m[ 1] = l[ 1] * r[ 0] + l[ 5] * r[ 1] + l[ 9] * r[ 2] + l[13] * r[ 3];
    m[ 2] = l[ 2] * r[ 0] + l[ 6] * r[ 1] + l[10] * r[ 2] + l[14] * r[ 3];
    m[ 3] = l[ 3] * r[ 0] + l[ 7] * r[ 1] + l[11] * r[ 2] + l[15] * r[ 3];

    m[ 4] = l[ 0] * r[ 4] + l[ 4] * r[ 5] + l[ 8] * r[ 6] + l[12] * r[ 7]; 
    m[ 5] = l[ 1] * r[ 4] + l[ 5] * r[ 5] + l[ 9] * r[ 6] + l[13] * r[ 7];
    m[ 6] = l[ 2] * r[ 4] + l[ 6] * r[ 5] + l[10] * r[ 6] + l[14] * r[ 7];
    m[ 7] = l[ 3] * r[ 4] + l[ 7] * r[ 5] + l[11] * r[ 6] + l[15] * r[ 7];
    
    m[ 8] = l[ 0] * r[ 8] + l[ 4] * r[ 9] + l[ 8] * r[10] + l[12] * r[11]; 
    m[ 9] = l[ 1] * r[ 8] + l[ 5] * r[ 9] + l[ 9] * r[10] + l[13] * r[11];
    m[10] = l[ 2] * r[ 8] + l[ 6] * r[ 9] + l[10] * r[10] + l[14] * r[11];
    m[11] = l[ 3] * r[ 8] + l[ 7] * r[ 9] + l[11] * r[10] + l[15] * r[11];
    
    m[12] = l[ 0] * r[12] + l[ 4] * r[13] + l[ 8] * r[14] + l[12] * r[15]; 
    m[13] = l[ 1] * r[12] + l[ 5] * r[13] + l[ 9] * r[14] + l[13] * r[15];
    m[14] = l[ 2] * r[12] + l[ 6] * r[13] + l[10] * r[14] + l[14] * r[15];
    m[15] = l[ 3] * r[12] + l[ 7] * r[13] + l[11] * r[14] + l[15] * r[15];
    memcpy(dst, m, sizeof(float) * 16);
}

void te_mat4_transpose(float *dst, const float *m) {
    float temp = m[1];
    dst[1] = m[2];
    dst[2] = temp;
    dst[0] = m[0];
    dst[3] = m[3];
}

void te_mat4_inverse(float *dst, const float *m) {
    /* TODO: implement lol*/
}
