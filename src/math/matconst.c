#include "linalg.h"

#include <math.h>

void te_mat4_orthographic(float *dst, float left, float right, float top, float bottom, float near, float far) {
    dst[ 0] =              2.0f / (right - left);
    dst[ 1] =                               0.0f;
    dst[ 2] =                               0.0f;
    dst[ 3] =   -(right + left) / (right - left);
    dst[ 4] =                               0.0f;
    dst[ 5] =              2.0f / (top - bottom);
    dst[ 6] =                               0.0f;
    dst[ 7] =   -(top + bottom) / (top - bottom);
    dst[ 8] =                               0.0f;
    dst[ 9] =                               0.0f;
    dst[10] =               -2.0f / (far - near);
    dst[11] =       -(far + near) / (far - near);
    dst[12] =                               0.0f;
    dst[13] =                               0.0f;
    dst[14] =                               0.0f;
    dst[15] =                               1.0f;
}

void te_mat3_translate(float *dst, float x, float y) {
    dst[0] =  0.0f;
    dst[1] =  0.0f;
    dst[2] =    x;
    dst[3] =  0.0f;
    dst[4] =  0.0f;
    dst[5] =    y;
    dst[6] =  0.0f;
    dst[7] =  0.0f;
    dst[8] =  1.0f;
}

void te_mat3_scale(float *dst, float x, float y) {
    dst[0] =    x;
    dst[1] =  0.0f;
    dst[2] =  0.0f;
    dst[3] =  0.0f;
    dst[4] =    y;
    dst[5] =  0.0f;
    dst[6] =  0.0f;
    dst[7] =  0.0f;
    dst[8] =  1.0f;
}

void te_mat3_rotate(float *dst, float rads) {
    float s = sin(rads),
          c = cos(rads);
    dst[0] =   c;
    dst[1] =  -s;
    dst[2] = 0.0f;
    dst[3] =   s;
    dst[4] =   c;
    dst[5] = 0.0f;
    dst[6] = 0.0f;
    dst[7] = 0.0f;
    dst[8] = 1.0f;
}

