#include "input.hpp"

#include <algorithm>

namespace te {

Input::Input() {
    keys    = new bool[TE_MAX_KEYS];
    buttons = new bool[TE_MAX_BUTTONS];
}

Input::~Input() {
    delete[] keys;
    delete[] buttons;
}

void Input::addKeyEvent(pfn_evtkey ek) { 
    evtkeys.push_back(ek);
}

void Input::addButtonEvent(pfn_evtbutton eb) {
    evtbuttons.push_back(eb);
}

void Input::addMouseEvent(pfn_evtmouse em) {
    evtmouse.push_back(em);
}

void Input::removeKeyEvent(pfn_evtkey ek) {
    evtkeys.erase(
        std::remove(evtkeys.begin(), evtkeys.end(), ek), 
        evtkeys.end()
    );
}

void Input::removeButtonEvent(pfn_evtbutton eb)  {
    evtbuttons.erase(
        std::remove(evtbuttons.begin(), evtbuttons.end(), eb), 
        evtbuttons.end()
    );
}

void Input::removeMouseEvent(pfn_evtmouse em) {
    evtmouse.erase(
        std::remove(evtmouse.begin(), evtmouse.end(), em), 
        evtmouse.end()
    );
}

bool Input::getKey(size_t _k) {
    return (_k > TE_MAX_KEYS) ? false : keys[_k];
}

bool Input::getButton(size_t _b) {
    return (_b > TE_MAX_BUTTONS) ? false : buttons[_b];
}

float Input::getMouseX() {
    return xmouse;
}

float Input::getMouseY() {
    return ymouse;
}

void Input::getMouse(float *_x, float *_y) {
    *_x = xmouse;
    *_y = ymouse;
}

}
