#ifndef TE_GRAPHICS_INPUT_HPP
#define TE_GRAPHICS_INPUT_HPP

#include <vector>
#include <cstddef>

namespace te {

#define TE_MAX_KEYS 512
#define TE_MAX_BUTTONS 32

typedef void (*pfn_evtkey)(size_t key, int action, void* userdata);
typedef void (*pfn_evtbutton)(size_t button, int action, void* userdata);
typedef void (*pfn_evtmouse)(float x, float y, void* userdata);

class Window;

class Input {
    private:
        bool*                        keys;
        bool*                        buttons;
        float                        xmouse;
        float                        ymouse;
        std::vector<pfn_evtkey>      evtkeys;
        std::vector<pfn_evtbutton>   evtbuttons;
        std::vector<pfn_evtmouse>    evtmouse;
    public:
        Input();
        ~Input();
    public:
        void  addKeyEvent(pfn_evtkey ek);
        void  addButtonEvent(pfn_evtbutton eb);
        void  addMouseEvent(pfn_evtmouse em);
        void  removeKeyEvent(pfn_evtkey ek);
        void  removeButtonEvent(pfn_evtbutton eb);
        void  removeMouseEvent(pfn_evtmouse em);
    public:
        bool  getKey(size_t _k);
        bool  getButton(size_t _b);
        float getMouseX();
        float getMouseY();
        void  getMouse(float *_x, float *_y);
    public:
        friend class Window;
};

}

#endif
