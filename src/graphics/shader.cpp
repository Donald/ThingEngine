#include "shader.hpp"
#include "../util/logger.h"

#include <glad/gl.h>

namespace te {


static bool createVertex(unsigned int *dst, const char *vertcode) {
    char info[512];
    int success;
    unsigned int vid = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vid, 1, &vertcode, NULL);
    te_logf(TE_LOG_NORMAL, "Compiling vertex shader...\n");
    glCompileShader(vid);
    glGetShaderiv(vid, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vid, 512, NULL, info);
        te_logf(TE_LOG_ERROR, "Vertex compilation failed:\n%s\n", info);
    }
    *dst = vid;
    return true;
}

static bool createFragment(unsigned int *dst, const char *fragcode) {
    char info[512];
    int success;
    unsigned int fid = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fid, 1, &fragcode, NULL);
    te_logf(TE_LOG_NORMAL, "Compiling fragment shader...\n");
    glCompileShader(fid);
    glGetShaderiv(fid, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fid, 512, NULL, info);
        te_logf(TE_LOG_ERROR, "Fragment compilation failed:\n%s\n", info);
    }
    *dst = fid;
    return true;
}

static bool createProgram(unsigned int *dst, unsigned int *vid, unsigned int *fid) {
    char info[512];
    int success;
    te_logf(TE_LOG_NORMAL, "Creating program...\n");
    unsigned int pid = glCreateProgram();
    glAttachShader(pid, *vid);
    glAttachShader(pid, *fid);
    glLinkProgram(pid);
    glGetProgramiv(pid, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(pid, 512, NULL, info);
        te_logf(TE_LOG_ERROR, "Program link failed:\n%s\n", info);
    }
    *dst = pid;
    return true;
}


Shader::Shader(const char *_vertcode, const char *_fragcode) { 
    te_logf(TE_LOG_NORMAL, "`te::Shader::Shader()`\n");
    unsigned int vid, fid;
    if(!createVertex(&vid, _vertcode))
        return;
    if(!createFragment(&fid, _fragcode)) {
        glDeleteShader(vid);
        return;
    }
    createProgram(&id, &vid, &fid);
    glDeleteShader(vid);
    glDeleteShader(fid);
}

Shader::~Shader() {
    glDeleteProgram(id);
}

void Shader::unbind() {
    glUseProgram(0);
}

void Shader::bind() {
    glUseProgram(id);
}


}

