#include "texture.hpp"
#include "src/util/logger.h"

#include <glad/gl.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#include <stdint.h>


namespace te {


Texture::Texture(const char *_filepath) {
    int channels;
    uint8_t *bytes = stbi_load(_filepath, &width, &height, &channels, 0);
    if(bytes == NULL) {
        te_logf(TE_LOG_ERROR, "Texture '%s' could not be loaded.\n", _filepath);
        return;
    }
    
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, (channels == 3) ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, bytes);
    glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(bytes);
}

Texture::~Texture() {
    glDeleteTextures(1, &id);
}

void Texture::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::bind() {
    glBindTexture(GL_TEXTURE_2D, id);
}

void Texture::bind(unsigned int index) {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, id);
}

int Texture::getWidth() const {
    return width;
}

int Texture::getHeight() const {
    return height;
}


}

