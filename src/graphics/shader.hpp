#ifndef TE_GRAPHICS_SHADER_HPP
#define TE_GRAPHICS_SHADER_HPP

namespace te {
    
    class Shader {
        private:
            unsigned int id;
        public:
            Shader() = delete;
            Shader(const char *_vertcode, const char *_fragcode);
            ~Shader();
        public:
            static void unbind();
                   void bind();
    };

}

#endif
