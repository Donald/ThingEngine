#ifndef TE_GRAPHICS_BATCHRENDER_HPP
#define TE_GRAPHICS_BATCHRENDER_HPP

#include "texture.hpp"

namespace te {


class BatchRender {
    private:
        Texture *sprites;
    public:
        BatchRender() = delete;
        BatchRender(Texture *sprites);
        ~BatchRender();
};


}

#endif
