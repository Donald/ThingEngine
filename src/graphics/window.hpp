#ifndef TE_GRAPHICS_WINDOW_HPP
#define TE_GRAPHICS_WINDOW_HPP

#include "input.hpp"

#include <vector>
#include <glad/gl.h>
#include <GLFW/glfw3.h>

namespace te {

class Window {
    private:
        GLFWwindow*         src;
        const char*         title;
        std::vector<Input*> inputs;
    public:
        Window();
        Window(int _w, int _h, const char* _title);
        ~Window();
    private:
        bool create(int _w, int _h, int _aspectn, int _aspectd, int _monitor, const char* _title);
    public:
        static void poll();
               void swap();
    public:
        void addInput(Input* _input);
        void removeInput(Input* _input);
    public:
        bool isOpen() const;
    private:
        static void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods);
        static void mouse_button_callback(GLFWwindow* win, int button, int action, int mods);
        static void cursor_position_callback(GLFWwindow* win, double xpos, double ypos);
};

}

#endif
