#ifndef TE_GRAPHICS_TEXTURE_HPP
#define TE_GRAPHICS_TEXTURE_HPP

namespace te {


class Texture {
    private:
        unsigned int id;
        int width;
        int height;
    public:
        Texture(const char *_filepath);
        ~Texture();
    public:
        static void unbind();
               void bind();
               void bind(unsigned int index);
    public:
        int getWidth() const;
        int getHeight() const;
};


}

#endif

