#include "window.hpp"
#include "../init.hpp"
#include "../util/logger.h"
#include "input.hpp"

#include <bson/bson.h>
#include <GLFW/glfw3.h>

#include <algorithm>
#include <cstdio>

namespace te {


Window::Window() {
    te_logf(TE_LOG_NORMAL, "`te::Window::Window()`\n");
    BsonLib* lib = te::libs()->bson;
    int monitor = 0, w = 800, h = 600, aspectn = 4, aspectd = 3;

    std::string configpath = te::libs()->exepath + "/config/display.bson";
    BsonResult res;
    BsonNode* broot = bson_file(configpath.c_str(), lib, &res);
    printf("%p %d\n", (void *)broot, res);
    if(broot == NULL) {
        te_logf(TE_LOG_ERROR, "config/display.bson is missing.\n");
        create(w, h, aspectn, aspectd, monitor, "ThingEngine - Check config/display.bson");
        return;
    }
    
    BsonNode* bmonitor = bson_get(broot, "monitor");
    BsonNode* baspect = bson_get(broot, "aspect");
    BsonNode* bsize = bson_get(broot, "size");

    if(bmonitor != NULL && bmonitor->type == BSON_TYPE_LNG)
        monitor = bmonitor->lng;
    else te_logf(TE_LOG_WARNING, "display/monitor is not int\n");
    
    if(baspect != NULL && baspect->type == BSON_TYPE_ARR && baspect->numchildren == 2) {
        if(baspect->arr[0].type == BSON_TYPE_LNG)
            aspectn = baspect->arr[0].lng;
        else te_logf(TE_LOG_WARNING, "display/aspect[0] is not int\n");
        if(baspect->arr[1].type == BSON_TYPE_LNG)
            aspectd = baspect->arr[1].lng;
        else te_logf(TE_LOG_WARNING, "display/aspect[1] is not int\n");
    }
    else te_logf(TE_LOG_WARNING, "display/aspect is not array\n");

    if(bsize != NULL && bsize->type == BSON_TYPE_ARR && bsize->numchildren == 2) {
        if(bsize->arr[0].type == BSON_TYPE_LNG)
            w = bsize->arr[0].lng;
        else te_logf(TE_LOG_WARNING, "display/size[0] is not int\n");
        if(bsize->arr[1].type == BSON_TYPE_LNG)
            h = bsize->arr[1].lng;
        else te_logf(TE_LOG_WARNING, "display/size[1] is not int\n");
    }
    else te_logf(TE_LOG_WARNING, "display/size is not array\n");
    
    bson_free(&broot, lib);

    create(w, h, aspectn, aspectd, monitor, "ThingEngine");
}

Window::Window(int _w, int _h, const char* _title) {
    create(_w, _h, -1, -1, -1, _title);
}

Window::~Window() {
    glfwDestroyWindow(src);
}

bool Window::create(int _w, int _h, int _aspectn, int _aspectd, int _monitor, const char* _title) {
    te_logf(
        TE_LOG_NORMAL, 
        "te::Window::create("
        "w: %d, h: %d, an: %d, ad: %d, m: %d, t: '%s')\n",
        _w, _h, _aspectn, _aspectd, _monitor, _title
    );
    
    src = glfwCreateWindow(_w, _h, _title, NULL, NULL);
    if(_aspectn > 0 && _aspectd > 0)
        glfwSetWindowAspectRatio(src, _aspectn, _aspectd);

    glfwSetWindowUserPointer(src, this);
    
    glfwSetKeyCallback(src, Window::key_callback);
    glfwSetMouseButtonCallback(src, Window::mouse_button_callback);
    glfwSetCursorPosCallback(src, Window::cursor_position_callback);
    
    glfwMakeContextCurrent(src);
    
    te_logf(TE_LOG_VERBOSE, "Loading OpenGL...\n");
    if(!gladLoadGL((GLADloadfunc)glfwGetProcAddress)) {
        glfwDestroyWindow(src);
        te_logf(TE_LOG_ERROR, "Could not load OpenGL (GLAD)\n");
        return false;
    }
    
    return true;
}

void Window::poll() {
    glfwPollEvents();
}

void Window::swap() {
    glfwSwapBuffers((GLFWwindow*) src);
}

void Window::addInput(Input* _input) {
    inputs.push_back(_input);
}

void Window::removeInput(Input* _input) {
    inputs.erase(
        std::remove(inputs.begin(), inputs.end(), _input),
        inputs.end()
    );
}

bool Window::isOpen() const {
    return !glfwWindowShouldClose((GLFWwindow*) src);
}

void Window::key_callback(GLFWwindow* win, int key, int scancode, int action, int mods) {
    Window* src = (Window*) glfwGetWindowUserPointer(win);
    size_t i, j;
    for(i = 0; i < src->inputs.size(); i++) {
        src->inputs[i]->keys[key] = action != GLFW_RELEASE;
        for(j = 0; j < src->inputs[i]->evtkeys.size(); j++)
            src->inputs[i]->evtkeys[j](key, action, NULL);
    }
}

void Window::mouse_button_callback(GLFWwindow* win, int button, int action, int mods) {
    Window* src = (Window*) glfwGetWindowUserPointer(win);
    size_t i, j;
    for(i = 0; i < src->inputs.size(); i++) {
        src->inputs[i]->buttons[button] = action != GLFW_RELEASE;
        for(j = 0; j < src->inputs[i]->evtbuttons.size(); j++)
            src->inputs[i]->evtbuttons[j](button, action, NULL);
    }
}

void Window::cursor_position_callback(GLFWwindow* win, double xpos, double ypos) {
    Window* src = (Window*) glfwGetWindowUserPointer(win);
    float x = xpos, y = ypos;
    size_t i, j;
    for(i = 0; i < src->inputs.size(); i++) {
        src->inputs[i]->xmouse = x;
        src->inputs[i]->ymouse = y;
        for(j = 0; j < src->inputs[i]->evtmouse.size(); j++)
            src->inputs[i]->evtmouse[j](x, y, NULL);
    }
}


}








