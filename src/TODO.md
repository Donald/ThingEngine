# TODO.md
- Finish texture
  - Manage between spritesheets with strict w/h/r/c and unstricted w/h/r/c
- 'Models'
  - Fully fledged models may not be required due to the (hopefully) full reliance on batch rendering.
- Text drawing
- Figure out:
  - Client in relation to server
    - Gameloop
    - Engine
