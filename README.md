# ThingEngine
## Description
This engine will implement 2D graphics and audio with ways to manage and load the game resources and game states. This project is powered by OpenGL. This engine is designed to make a game that I have in mind. This will be my first proper game with it an engine so this repository is a learning experience for my self.

## Notes
In development. Not intended for use yet, but source is open and source my guys.
## Building
Dependencies:
- GLFW: For window management
- GLAD: For OpenGL loading
- CLang/CLang++: Compiler
- BSON: Configuration files
Why GLFW?
- Too easy to make and manage window
Why GLAD?
- GLEW behaves weird and GLAD seems more stable and I can easily load the most recent API.
- I am not a fan of how GLAD works and I would have preferred to use Vulkan, but Vulkan is too much of a headache to learn at this time in my life due to external factors.
- With GLAD, I do not like adding external C files as a library. At the moment, I compiled gl.c from glad and put the .so file into /usr/local. I will not do this for a release version as that complicates everything.
Why CLang?
- Seeing if I like it more than GCC.
What's BSON?
- A config thing I made because I hate JSON. I know 'BSON' already exists, I don't care, I hate JSON
