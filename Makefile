CPP = clang++
CC  = clang
LD  = clang

DIR_SRC = src
DIR_BIN = bin

SOURCES_CPP = $(shell find $(DIR_SRC) -type f -name '*.cpp')
SOURCES_CC  = $(shell find $(DIR_SRC) -type f -name '*.c')
OBJECTS_CPP = $(addprefix $(DIR_BIN)/, $(notdir $(SOURCES_CPP:.cpp=.o))) 
OBJECTS_CC  = $(addprefix $(DIR_BIN)/, $(notdir $(SOURCES_CC:.c=.o)))

CFLAGS = -I. -O3 -Wall -Wpedantic -Werror -Wno-unused -Isrc
LFLAGS = -Wall -lglfw -lglad -lbson -lm -lstdc++

TARGET = $(DIR_BIN)/run

all: $(OBJECTS) $(TARGET)

$(OBJECTS_CPP): $(DIR_BIN)/%.o: $(SOURCES_CPP)
	$(CPP) -c $^ $(CFLAGS)
	@mv *.o $(DIR_BIN)/

$(OBJECTS_CC): $(DIR_BIN)/%.o: $(SOURCES_CC)
	$(CC) -c $^ $(CFLAGS)
	@mv *.o $(DIR_BIN)/

$(TARGET): $(OBJECTS_CPP) $(OBJECTS_CC)
	$(LD) -o $@ $(OBJECTS_CC) $(OBJECTS_CPP) $(LFLAGS)

clean:
	rm -f $(DIR_BIN)/*.o

reset: clean
	rm -f $(TARGET)
